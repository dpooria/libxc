/*
 Copyright (C) 2006-2007 M.A.L. Marques

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/


#include "util.h"

/************************************************************************
 Implements Tao, Perdew, Staroverov & Scuseria
   meta-Generalized Gradient Approximation.

  Exchange part
************************************************************************/


#define XC_MGGA_X_ISOTHETA 800 /*Piotr*/

typedef struct{
  double b, c, e, kappa, mu;
  double BLOC_a, BLOC_b;
} mgga_x_isotheta_params;


static void
mgga_x_isotheta_init(xc_func_type *p)
{
  assert(p!=NULL && p->params == NULL);
  p->params = libxc_malloc(sizeof(mgga_x_isotheta_params));
}

#define ISOTHETA_N_PAR 7
static const char  *isotheta_names[ISOTHETA_N_PAR]  = {"_b", "_c", "_e", "_kappa", "_mu", "_BLOC_a", "_BLOC_b"};
static const char  *isotheta_desc[ISOTHETA_N_PAR]   = {
  "b", "c", "e",
  "Asymptotic value of the enhancement function",
  "Coefficient of the 2nd order expansion",
  "BLOC_a", "BLOC_b"
};
static const double isotheta_values[ISOTHETA_N_PAR] = {
  0.40, 1.59096, 1.537, 0.8040, 0.21951, 2.0, 0.0
};

#include "maple2c/mgga_exc/mgga_x_isotheta.c"
#include "work_mgga.c"

#ifdef __cplusplus
extern "C"
#endif
const xc_func_info_type xc_func_info_mgga_x_isotheta = {
  XC_MGGA_X_ISOTHETA,
  XC_EXCHANGE,
  "Tao, Perdew, Staroverov & Scuseria",
  XC_FAMILY_MGGA,
  {&xc_ref_Tao2003_146401, &xc_ref_Perdew2004_6898, NULL, NULL, NULL},
  XC_FLAGS_3D | XC_FLAGS_NEEDS_TAU | MAPLE2C_FLAGS,
  1e-15,
  {ISOTHETA_N_PAR, isotheta_names, isotheta_desc, isotheta_values, set_ext_params_cpy},
  mgga_x_isotheta_init, NULL,
  NULL, NULL, &work_mgga,
};

