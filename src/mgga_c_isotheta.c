/*
 Copyright (C) 2006-2007 M.A.L. Marques

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/


#include "util.h"

#define XC_MGGA_C_ISOTHETA 801 /*Piotr*/

typedef struct{
  double beta, d;
  double C0_c[4];
} mgga_c_isotheta_params;

static void
mgga_c_isotheta_init(xc_func_type *p)
{
  assert(p != NULL && p->params == NULL);
  p->params = libxc_malloc(sizeof(mgga_c_isotheta_params));
}

#define ISOTHETA_N_PAR 6
static const char  *isotheta_names[ISOTHETA_N_PAR]  = {"_beta", "_d", "_C0_c0", "_C0_c1", "_C0_c2", "_C0_c3"};
static const char  *isotheta_desc[ISOTHETA_N_PAR]   = {"beta", "d", "C0_c0", "C0_c1", "C0_c2", "C0_c3"};
static const double isotheta_values[ISOTHETA_N_PAR] = {
  0.06672455060314922, 2.8, 0.53, 0.87, 0.50, 2.26
};

#include "maple2c/mgga_exc/mgga_c_isotheta.c"
#include "work_mgga.c"

#ifdef __cplusplus
extern "C"
#endif
const xc_func_info_type xc_func_info_mgga_c_isotheta = {
  XC_MGGA_C_ISOTHETA,
  XC_CORRELATION,
  "Tao, Perdew, Staroverov & Scuseria",
  XC_FAMILY_MGGA,
  {&xc_ref_Tao2003_146401, &xc_ref_Perdew2004_6898, NULL, NULL, NULL},
  XC_FLAGS_3D | XC_FLAGS_NEEDS_TAU | MAPLE2C_FLAGS,
  1e-15, /* densities smaller than 1e-26 give NaNs */
  {ISOTHETA_N_PAR, isotheta_names, isotheta_desc, isotheta_values, set_ext_params_cpy},
  mgga_c_isotheta_init, NULL,
  NULL, NULL, &work_mgga,
};
